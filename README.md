# Input

Custom files I use for configuring input on my linux computers. Feel free to use!

## Scientific Keyboard

Extends the traditional US keyboard with a third level shift on the right alt key, adding greek letters, superscript & subscript numbers, and mathematical symbols commonly used in science and engineering.

Files:
- sci: just the scientific keyboard
- us: modified version of the US keyboard file, replacing the default US keyboard with the custom scientific keyboard.

### Installation
- The easiest way to install the scientific keyboard is to download "us" and copy it into /usr/share/X11/xkb/symbols, overwriting the version there. This will replace the default US keyboard with the scientific keyboard. Restart your computer and you're good to go!
- Alternatively, choose a different keyboard you'd like to replace (ex: Icelandic Dvorak), and copy the keybindings (between the dashed lines) from "sci" over the keybindings for that keyboard.
- You could also try just copying sci into /usr/share/X11/xkb/symbols, but I haven't had much luck with that method. The file tends not to be recognized by system settings.

## License

All files in this repository are distributed under the CC0 license, with the following exceptions:
- the "us" file, aside from my modifications, is covered by the X11 license.
